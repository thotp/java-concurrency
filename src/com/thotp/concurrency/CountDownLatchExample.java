package com.thotp.concurrency;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchExample {

	public static void main(String[] args) {
		
		final CountDownLatch startLatch = new CountDownLatch(1);
		final CountDownLatch finishLatch = new CountDownLatch(10);
		
		for (int i = 0; i < 10; i++) {
			new JobWorker("JobWorker-"+i, startLatch, finishLatch).start();
			System.out.println("Job Worker "+i+" started");
		}
		System.out.println("Job Worker Threads waiting for Main Thread to initialize environment...");
		try {			
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Main Thread finished intializing environment.");
		startLatch.countDown();
		System.out.println("Main Thread waiting for all workers to finish...");
		try {
			finishLatch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Main Thread finish.");
	}
	
	static class JobWorker extends Thread {
		private final CountDownLatch startLatch;
		private final CountDownLatch finishLatch;
		public JobWorker(String name, CountDownLatch startLatch, CountDownLatch finishLatch) {
			super(name);
			this.startLatch = startLatch;
			this.finishLatch = finishLatch;
		}
		
		@Override
		public void run() {
			try {
				startLatch.await();
				System.out.println(Thread.currentThread().getName()+" performs job...");
				Thread.sleep(1000);
				System.out.println(Thread.currentThread().getName()+" finished.");
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				finishLatch.countDown();
			}
		}
	}
	
	

}
