package com.thotp.concurrency;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConditionObjectExample {

	public static void main(String[] args) {
		ReentrantLock lock = new ReentrantLock();
		Condition canPing = lock.newCondition();
		Condition canPong = lock.newCondition();
		new PingThread(lock, canPing, canPong).start();
		new PongThread(lock, canPing, canPong).start();
	}
	
	static class PingThread extends Thread {
		
		private final Lock lock;
		private final Condition canPing;
		private final Condition canPong;
		
		public PingThread(Lock lock, Condition canPing, Condition canPong) {
			this.lock = lock;
			this.canPing = canPing;
			this.canPong = canPong;
		}

		@Override
		public void run() {
			while (true){
				lock.lock();
				try {
					canPong.signal();			
					canPing.await();
					System.out.println("Ping");					
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					lock.unlock();
				}
			}
		}		
	}
	
	static class PongThread extends Thread {
		
		private final Lock lock;
		private final Condition canPing;
		private final Condition canPong;
		
		public PongThread(Lock lock, Condition canPing, Condition canPong) {
			this.lock = lock;
			this.canPing = canPing;
			this.canPong = canPong;
		}
		
		@Override
		public void run() {
			while (true){
				lock.lock();
				try {
					canPing.signal();			
					canPong.await();
					System.out.println("Pong");					
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					lock.unlock();
				}
			}
		}
	}

}
