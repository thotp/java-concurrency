package com.thotp.concurrency;

import java.util.concurrent.locks.ReentrantLock;


public class ReentrantLockExample {
	
	public static void main(String[] args) {

		final Resource resource = new Resource();
		
		Thread worker1 = new Thread() {
			@Override
			public void run() {
				for (int i = 0; i < 10; i++) {
					resource.increaseLong();
				}
			};
		};
		Thread worker2 = new Thread() {
			@Override
			public void run() {
				for (int i = 0; i < 10; i++) {
					resource.decreaseLong();
				}
			}
		};
		worker1.start();
		worker2.start();
	}
	
	static class Resource {
		
		// Change fairness between true and false to see the effect
		private final ReentrantLock lock = new ReentrantLock(true);
		
		private int value;
		
		void increaseLong(){
			lock.lock();			
			try {
				System.out.println("Increase value by 1: "+ ++value);
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				lock.unlock();
			}
		}
		
		void decreaseLong(){
			lock.lock();
			try {
				System.out.println("Decrease value by 1: "+ --value);
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				lock.unlock();
			}
		}
	}

}
