package com.thotp.concurrency;

import java.util.concurrent.Semaphore;

public class SemaphoreExample {

	public static void main(String[] args) {
		
		final Resource resource = new Resource();
		
		class JobRequester extends Thread {
			public JobRequester(String name) {
				super(name);
			}
			@Override
			public void run() {
				resource.doWorkAsync();
			}
		}
		
		for (int i = 0; i < 10; i++) {
			new JobRequester("JobRequester-"+i).start();
		}
	}
	
	static class Resource {
		
		// Change fairness between TRUE and FALSE to see the effect
		private final Semaphore semaphore = new Semaphore(1, false);
		
		void doWorkAsync() {
			System.out.println(Thread.currentThread().getName()+": waiting for semaphore...");
			try {
				semaphore.acquire();
				System.out.println(Thread.currentThread().getName()+": acquired semaphore. Start job worker...");
				new JobWorker(Thread.currentThread().getName()+"-JobWorker").start();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}			
		}
		
		class JobWorker extends Thread {
			public JobWorker(String name) {
				super(name);
			}
			@Override
			public void run() {
				System.out.println(Thread.currentThread().getName()+": executing job...");
				try {
					Thread.sleep(5000);
					System.out.println(Thread.currentThread().getName()+": release semaphore.");
					semaphore.release();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
