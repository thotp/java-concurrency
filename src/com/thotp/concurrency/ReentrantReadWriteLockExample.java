package com.thotp.concurrency;

import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReentrantReadWriteLockExample {

	public static void main(String[] args) {
		
		final Resource resource = new Resource();
		
		class ReadThread extends Thread {
			public ReadThread(String name) {
				super(name);
			}
			@Override
			public void run() {
				for (int i = 0; i < 10; i++) {
					resource.read();
				}
			}
		}
		class WriteThread extends Thread {
			public WriteThread(String name) {
				super(name);
			}
			@Override
			public void run() {
				for (int i = 0; i < 10; i++) {
					resource.write();
				}
			}
		}
		for (int i = 0; i < 100; i++) {
			new ReadThread("Read-"+i).start();
		}
		for (int i = 0; i < 10; i++) {
			new WriteThread("Write-"+i).start();
		}
	}
	
	static class Resource {		
		private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock(true);
		private int value = 0;
		
		void read(){			
			lock.readLock().lock();
			try {
				System.out.println(Thread.currentThread().getName()+": value=" + value);
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				lock.readLock().unlock();
			}
		}
		
		void write(){
			lock.writeLock().lock();
			try {
				System.out.println(Thread.currentThread().getName()+": value=" + ++value);
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				lock.writeLock().unlock();
			}
		}
	}

}
